/* jshint esversion: 6 */
Vue.use(VeeValidate, {
    events: ''
});
$(function() {
    Vue.component('quote-answers', {
        template: '#quote-answers-template',
        data: function() {
            return {
                showError: false
            };
        },
        props: ['step', 'stepRules', 'quoteResults', 'answers', 'PurchaseOnly', 'SalePurchase', 'SaleOnly', 'Remortgage', 'clearError', 'showError'],
        methods: {
            /* getAnswerLabel: function(answer) {
                if (Array.isArray(answer)) {
                    return answer[1];
                }
                return answer;
            },
            getAnswerValue: function(answer) {
                if (Array.isArray(answer)) {
                    return answer[0];
                }
                return answer;
            }, */
            /*  clearError(){

                 return showError = false;
                 if (step === 3 && quoteResults[3] == 'Yes'){
                     return banks = true


                 } else {
                     return banks = false

                 }
                 console.log(quoteResults[3][0])

             } */
        }
    });
    const app = new Vue({
        el: "#app",
        data() {
            return {
                step: 1,
                id: 1,
                stepRules: true,
                PurchaseOnly: false,
                SalePurchase: false,
                SaleOnly: false,
                Remortgage: false,
                selected: '',
                quoteResults: {},
                showError: false,
                showErrorBanks: false,
                showBanks: true,
                showErrorExtraInfo: false,
                showExtraInfo: true,
                extraInfo: '',
                aboutMe: {},
                name: null,
                age: null,
                movie: null,
                countSteps: 1,
                stepsPO: 14,
                stepsSP: 20,
                stepsON: 10,
                stepsRM: 9,
                widthPercent: '',
                changeFooterClass: false,
                showSubmitPage: false,
                questions: [{}, {}, {
                    id: 2,
                    PurchaseOnly: 'Freehold or leasehold on the property you are <strong><u>buying</strong></u>?',
                    SalePurchase: 'Freehold or leasehold on the property you are <strong><u>selling</strong></u>?',
                    SaleOnly: 'Freehold or leasehold on the property you are <strong><u>selling</strong></u>?',
                    Remortgage: 'Freehold or leasehold on the property you are remortgaging?'
                }, {
                    id: 3,
                    PurchaseOnly: 'Are you obtaining a mortgage?',
                    SalePurchase: 'Is there a mortgage on the property you are <strong><u>selling</strong></u>?',
                    SaleOnly: 'Is there a mortgage on the property you are <strong><u>selling</strong></u>?',
                }, {
                    id: 4,
                    PurchaseOnly: 'Are you a first time buyer?',
                    SalePurchase: 'Is the property you are <strong><u>selling</strong></u> owned via the Shared Ownership Scheme?',
                    SaleOnly: 'Is the property you are <strong><u>selling</strong></u> owned via the Shared Ownership Scheme?',
                }, {
                    id: 5,
                    PurchaseOnly: 'Number of buyers',
                    SalePurchase: 'Number of sellers',
                    SaleOnly: 'Number of sellers',
                    Remortgage: 'Number of people remortgaging?'
                }, {
                    id: 6,
                    PurchaseOnly: 'Is the property a newbuild?',
                    SalePurchase: 'Price of property you are <strong><u>selling</strong></u> ',
                    SaleOnly: 'Price of property you are <strong><u>selling</strong></u> ',
                    Remortgage: 'Is it a buy to let?'
                }, {
                    id: 7,
                    PurchaseOnly: 'Is this going to be an additional purchase / second home?',
                    SalePurchase: 'Postcode of property you are <strong><u>selling</strong></u> ',
                    SaleOnly: 'Postcode of property you are <strong><u>selling</strong></u> ',
                    Remortgage: 'Value of property being remortgaged'
                }, {
                    id: 8,
                    PurchaseOnly: 'Are you using any of the following purchase schemes?',
                    SalePurchase: 'Freehold or leasehold on the property you are buying?',
                    SaleOnly: 'Postcode of property you are selling',
                    Remortgage: 'Postcode of property being remortgaged'
                }, {
                    id: 9,
                    PurchaseOnly: 'Is this going to be an additional purchase / second home?',
                    SalePurchase: 'Are you obtaining a mortgage on the property you are buying?',
                    SaleOnly: '',
                    Remortgage: 'When are you looking to instruct?'
                }, {
                    id: 10,
                    PurchaseOnly: 'Price of property you are buying',
                    SalePurchase: 'Number of buyers',
                    SaleOnly: '',
                    Remortgage: 'Do you want to provide any extra information?'
                }, {
                    id: 11,
                    PurchaseOnly: 'When are you looking to instruct?',
                    SalePurchase: 'Is the property you are  <strong><u>buying </strong></u> a newbuild?',
                    SaleOnly: '',
                    Remortgage: 'About me'
                }, {
                    id: 12,
                    PurchaseOnly: 'Do you want to provide any extra information?',
                    SalePurchase: 'Is the property you are <strong><u>buying </strong></u> a <i>buy to let</i>?',
                    SaleOnly: '',
                    Remortgage: 'Number of people remortgaging?'
                }, {
                    id: 13,
                    PurchaseOnly: 'About me',
                    SalePurchase: 'Is this going to be an additional purchase / second home?',
                    SaleOnly: '',
                    Remortgage: 'Number of people remortgaging?'
                }, {
                    id: 14,
                    PurchaseOnly: 'For the property you are buying, are you using the Shared Ownership scheme?',
                    SalePurchase: 'For the property you are <strong><u>buying </strong></u>, are you using the Shared Ownership scheme?',
                    SaleOnly: '',
                    Remortgage: 'Number of people remortgaging?'
                }, {
                    id: 15,
                    PurchaseOnly: 'Are you using any of the following purchase schemes?',
                    SalePurchase: 'Are you using any of the following purchase schemes?',
                    SaleOnly: '',
                    Remortgage: 'Number of people remortgaging?'
                }, {
                    id: 16,
                    PurchaseOnly: 'Price of property you are <strong><u>buying </strong></u>',
                    SalePurchase: 'Price of property you are <strong><u>buying </strong></u>',
                    SaleOnly: '',
                    Remortgage: 'Number of people remortgaging?'
                }, {
                    id: 17,
                    PurchaseOnly: 'Postcode of property you are <strong><u>buying </strong></u>',
                    SalePurchase: 'Postcode of property you are <strong><u>buying </strong></u>',
                    SaleOnly: '',
                    Remortgage: 'Number of people remortgaging?'
                }, {
                    id: 18,
                    PurchaseOnly: 'When are you looking to instruct?',
                    SalePurchase: 'When are you looking to instruct?',
                    SaleOnly: 'When are you looking to instruct?',
                    Remortgage: 'When are you looking to instruct?'
                }, {
                    id: 19,
                    PurchaseOnly: 'Do you want to provide any extra information?',
                    SalePurchase: 'Do you want to provide any extra information?',
                    SaleOnly: 'Do you want to provide any extra information?',
                    Remortgage: 'Do you want to provide any extra information'
                }, {
                    id: 20,
                    PurchaseOnly: 'About me',
                    SalePurchase: 'About me',
                    SaleOnly: 'About me',
                    Remortgage: 'About me'
                }],
                answers: [
                    //using steps as index, starting from 1
                    ['Purchase only', 'Sale and purchase', 'Sale only', 'Remortgage'],
                    ['Leasehold', 'Freehold'],
                    ['Yes', 'No'],
                    ['Yes', 'No'],
                    ['Just me', 'Me and one other', '3 people', '4 people', '5 people', '6 people'],
                    ['Yes', 'No'],
                    ['Yes', 'No'],
                    ['Leasehold', 'Freehold'],
                    ['Yes', 'No'],
                    ['Just me', 'Me and one other', '3 people', '4 people', '5 people', '6 people'],
                    ['Yes', 'No'],
                    ['Yes', 'No'],
                    ['Yes', 'No'],
                    ['Yes', 'No'],
                    ['Not using purchase scheme', 'Help to buy ISA', 'Help to buy', 'Right to buy'],
                    ['16', 'xx'],
                    ['17', 'xx'],
                    ['Within 1 week', 'Within 1 month', 'Within 3 months', 'Not sure'],
                    ['Yes', 'No'],
                    ['20', 'xx'],
                ],
                results: [{
                    'name': 'My Home Move Conveyancing',
                    'img_src': 'images/conv_image.png',
                    'fees': {
                        'legal': 200.0,
                        'vat': 220.0,
                        'disbursements': 100.00
                    },
                    'total': 1100.0,
                    'actions': 'http://bbc.co.uk'
                }, {
                    'name': 'My Home Move Conveyancing',
                    'img_src': 'images/conv_image.png',
                    'fees': {
                        'legal':1200.0,
                        'vat': 1220.0,
                        'disbursements': 1100.00
                    },
                    'total': 1100.0,
                    'actions': 'http://bbc.co.uk'
                }],
            };
        },
        methods: {
            next() {
                if (this.quoteResults[this.step] !== undefined && this.quoteResults[this.step].length) {
                    this.showError = false;
                    this.step++;
                    this.countSteps++;
                    /* uncomment this below to get to the end faster */
                    /* if (this.step === 2) {
                        this.step = 20;
                    } */
                    //the whole form depends on the first question answer, then we control what steps show the content here
                    if (this.quoteResults[1] === 'Purchase only') {
                        this.PurchaseOnly = true;
                        this.widthPercent = parseInt(this.countSteps * (100 / this.stepsPO) + 2);
                        if (this.step === 8) {
                            this.step = 14;
                        } else if (this.step === 16 || this.step === 17 || this.step === 20) {
                            this.stepRules = false;
                        } else {
                            this.stepRules = true;
                        }
                    } else if (this.quoteResults[1] === 'Sale and purchase') {
                        this.SalePurchase = true;
                        this.widthPercent = parseInt(this.countSteps * (100 / this.stepsSP) + 2);
                        if (this.step === 6 || this.step === 7 || this.step === 16 || this.step === 17 || this.step === 20) {
                            this.stepRules = false;
                        } else if (this.step === 8) {
                            this.stepRules = true;
                        } else {
                            this.stepRules = true;
                        }
                    } else if (this.quoteResults[1] === 'Sale only') {
                        this.SaleOnly = true;
                        this.widthPercent = parseInt(this.countSteps * (100 / this.stepsON) + 2);
                        if (this.step === 6 || this.step === 7 || this.step === 20) {
                            this.stepRules = false;
                        } else if (this.step === 8) {
                            this.stepRules = true;
                            this.step = 18;
                        } else {
                            this.stepRules = true;
                        }
                    } else if (this.quoteResults[1] === 'Remortgage') {
                        this.Remortgage = true;
                        this.widthPercent = parseInt(this.countSteps * (100 / this.stepsRM) + 2);
                        if (this.step === 3) {
                            this.step = 5;
                        } else if (this.step === 7 || this.step === 8 || this.step === 20) {
                            this.stepRules = false;
                        } else if (this.step === 9) {
                            this.stepRules = true;
                            this.step = 18;
                        } else {
                            this.stepRules = true;
                        }
                    }
                } else {
                    this.showError = true;
                }
            },
            prev() {
                this.showError = false;
                this.step--;
                this.countSteps--;
                if (this.step === 1) {
                    this.PurchaseOnly = false;
                    this.SalePurchase = false;
                    this.SaleOnly = false;
                    this.Remortgage = false;
                }
                //add stepRules for each section (first answer route)
                if (this.PurchaseOnly) {
                    this.widthPercent = parseInt(this.countSteps * (100 / this.stepsPO) + 2);
                    if (this.step === 13) {
                        this.step = 7;
                    } else if (this.step === 16 || this.step === 17 || this.step === 20) {
                        this.stepRules = false;
                    } else {
                        this.stepRules = true;
                    }
                }
                if (this.SalePurchase) {
                    this.widthPercent = parseInt(this.countSteps * (100 / this.stepsSP) + 2);
                    if (this.step === 6 || this.step === 7 || this.step === 16 || this.step === 17) {
                        this.stepRules = false;
                    } else {
                        this.stepRules = true;
                    }
                }
                if (this.SaleOnly) {
                    this.widthPercent = parseInt(this.countSteps * (100 / this.stepsSO) + 2);
                    if (this.step === 6 || this.step === 7) {
                        this.stepRules = false;
                    } else if (this.step === 17) {
                        this.stepRules = false;
                        this.step = 7;
                    } else {
                        this.stepRules = true;
                    }
                }
                if (this.Remortgage) {
                    this.widthPercent = parseInt(this.countSteps * (100 / this.stepsRM) + 2);
                    if (this.step === 4) {
                        this.step = 2;
                    } else if (this.step === 7 || this.step === 8 || this.step === 20) {
                        this.stepRules = false;
                    } else if (this.step === 17) {
                        this.stepRules = false;
                        this.step = 8;
                    } else {
                        this.stepRules = true;
                    }
                }
            },
            onchange() {
                if (this.step === 3 && this.PurchaseOnly || this.step === 9 && this.SalePurchase) {
                    if (this.selected === '') {
                        this.showErrorBanks = true;
                    } else {
                        this.showErrorBanks = false;
                        this.showBanks = false;
                    }
                } else if (this.step === 19 && this.PurchaseOnly || this.step === 19 && this.SalePurchase || this.step === 19 && this.SaleOnly || this.step === 19 && this.Remortgage) {
                    if (this.extraInfo === '') {
                        this.showErrorExtraInfo = true;
                        this.showExtraInfo = true;
                    } else {
                        this.showErrorExtraInfo = false;
                        this.showExtraInfo = false;
                    }
                }
            },
            submit() {
                this.changeFooterClass = true;
                this.$validator.validateAll().then((result) => {
                    if (result) {
                        // eslint-disable-next-line
                        this.showSubmitPage = true;
                        this.changeFooterClass = false;
                    }
                });
            }
        },
        computed: {
            numberSteps: function() {
                if (this.PurchaseOnly) {
                    return this.stepsPO;
                } else if (this.SalePurchase) {
                    return this.stepsSP;
                } else if (this.SaleOnly) {
                    return this.stepsON;
                } else if (this.Remortgage) {
                    return this.stepsRM;
                }
            }
        }
    });
});
